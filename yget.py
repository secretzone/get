import youtube_dl
from youtube_dl import DownloadError
import glob, os, shutil

filename = 'gety.txt'
out_folder = 'videos'
ydl_opts = {'retries': 10}

with open(filename) as f:
    lines = [line.rstrip('\n') for line in f]

for line in lines:
    try:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([line])
    except DownloadError as e:
        print("couldn't download {0}".format(line))



if not os.path.exists(out_folder):
    os.makedirs(out_folder)

source_dir = '.'
files = glob.iglob(os.path.join(source_dir, "*.mp4"))
for file in files:
    if os.path.isfile(file):
        shutil.move(file, out_folder)

